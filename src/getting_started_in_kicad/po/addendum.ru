PO4A-HEADER: mode=after; position=^\[\[contributors\]\]; beginboundary=\[\[
[[translation]]
*Перевод*

//Translators put your names below here in the addendum file
Викулов Ю.Н. <boxforvik@mail.ru> - русская версия (с использованием пошагового руководства по kicad (C) 2010) +
Юрий Козлов <yuray@komyakino.ru>, 2016 +
alex9 <gmdii@mail.ru>, 2016 +
Барановский Константин <baranovskiykonstantin@gmail.com>, 2016-2021
